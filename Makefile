# API
migrate:
	docker-compose exec php php artisan migrate

migrate-fresh:
	docker-compose exec php php artisan migrate:fresh

reset-api:
	make migrate-fresh
	docker-compose exec php php artisan cache:clear
	docker-compose exec php php artisan key:generate
	docker-compose exec php php artisan db:seed

# Web
restart-web:
	docker-compose restart node
	make attach-web

attach-web:
	docker-compose logs -f --no-log-prefix --tail=10 node

# Run the project
up:
	make up-and-wait
	make migrate

up-and-seed:
	make up-and-wait
	make reset-api

up-and-wait:
	docker-compose up -d
	@while [ -z "$$(docker-compose logs database | grep -o "ready to accept connections")"]; \
	do \
		sleep 1; \
	done

# Installation
setup-https:
	mkcert -install
	cd docker/caddy/certs && mkcert elerium.localhost
	cd docker/caddy/certs && mkcert api.elerium.localhost
	ln -s $(mkcert -CAROOT) ./docker/CA

install:
	cd api && composer install --ignore-platform-reqs
	cd api && cp .env.example .env
	cd api && php artisan key:generate
	cd web && npm install
	make setup-https
	make up-and-wait
	make migrate
	docker-compose stop
