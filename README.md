# Elerium
## Prérequis
- docker & docker-compose
- php
- composer
- npm
- [mkcert](https://github.com/FiloSottile/mkcert#installation)

## Installation

Clonez le projet et récupérez les submodules
~~~ sh
git clone git@gitlab.com:FooTeam/elerium/elerium-env.git --recurse-submodules
~~~

Initialisez le projet
~~~ sh
make install
~~~

## Commandes spéciales

Pour exécuter les migrations :
~~~ sh
docker-compose exec php php artisan migrate # make migrate
~~~

Pour redémarrer le serveur node :
~~~ sh
docker-compose restart node # make restart-web
~~~

Pour suivre les logs du serveur node :
~~~ sh
docker-compose logs -f node # make attach-web
~~~

